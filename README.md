Eye Agent
=========

A program to record URL reachability and to send it
to another program called Eye Office for long-term storage.

Features
--------

* Recording HTTP and HTTPS URLs.
* Recording multiple URLs simulteneously.
* Recording URL status code and headers.
* Recording in every minute.

Configuration
-------------

`config.js` contains the configuration.

Scripts
-------

* `./restart.sh` - start/restart the program.
* `./stop.sh` - stop the program.
* `./clean.sh` - clean the program after an unexpected shutdown.
* `./rotate.sh` - clean old logs.
