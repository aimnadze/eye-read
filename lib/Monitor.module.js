module.exports = app => () => {

    function next () {
        date.setUTCMinutes(date.getUTCMinutes() + 1)
        app.Timeout(() => {
            ;(urls => {
                app.Parallel(urls.map(url => done => {
                    app.Fetch(url, result => {
                        done({ url, result })
                    })
                }), urls => {
                    send_queue({
                        time: date.getTime() / 1000,
                        urls,
                    })
                    next()
                })
            })(urls.slice())
        }, date.getTime() - Date.now())
    }

    let urls = []

    const send_queue = app.SendQueue(_urls => {
        urls = _urls
    })

    const date = new Date
    date.setUTCMilliseconds(0)
    date.setUTCSeconds(0)

    next()

}
