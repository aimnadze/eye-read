module.exports = (value, nullable) => {
    const number = parseInt(value, 10)
    return isFinite(number) ? Math.abs(number) : (nullable ? null : 0)
}
