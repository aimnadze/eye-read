const http = require('http')
const https = require('https')
const url = require('url')

module.exports = app => (fetch_url, done) => {

    const log = app.log.sublog('Fetch ' + fetch_url)
    log.info('Begin')

    const parsed_url = url.parse(fetch_url)

    const module = parsed_url.protocol === 'http:' ? http : https

    ;(done => {
        ;(error => {

            const req = module.get({
                host: parsed_url.host,
                port: parsed_url.port,
                path: parsed_url.path,
            }, res => {
                req.abort()
                abort_timeout()
                done({
                    error: '',
                    status_code: res.statusCode,
                    headers: res.headers,
                })
            })
            req.on('error', err => {
                abort_timeout()
                error(err)
            })

            const abort_timeout = app.Timeout(() => {
                req.abort()
                error({ code: 'TIMEOUT' })
            }, 1000 * 10)

        })(err => {
            done({
                error: err.code,
                status_code: 0,
                headers: {},
            })
        })
    })(result => {
        log.info('End')
        done(result)
    })

}
